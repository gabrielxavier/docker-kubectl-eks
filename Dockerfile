FROM debian:bookworm-slim
LABEL maintainer = "Gabriel Xavier | https://gabrielxavier.com"

RUN set -ex; \
    TempBuildDeps=" \
        apt-transport-https \
        ca-certificates \
        curl \
        wget \
        unzip \
        software-properties-common \
    "; \
    apt update; \
    apt install --no-install-recommends -qy python3-pip $TempBuildDeps; \
    \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"; \
    unzip awscliv2.zip; ./aws/install; \
    rm -rf ./aws/; rm -f ./awscliv2.zip; \
    \
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"; \
    chmod 755 ./kubectl; \
    mv ./kubectl /usr/local/bin/kubectl; \
    \
    curl -Lo aws-iam-authenticator $(curl -s https://api.github.com/repos/kubernetes-sigs/aws-iam-authenticator/releases/latest | grep browser_download_url | grep linux | cut -d '"' -f 4); \
    chmod 755 ./aws-iam-authenticator; \
    mv ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator; \
    \
    curl -fsSL https://goss.rocks/install | sh; \
    curl -fsSL https://get.docker.com | sh; \
    \
    apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*;
    # apt-get purge -y --auto-remove $TempBuildDeps

CMD ["/bin/bash"]
